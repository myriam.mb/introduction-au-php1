<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php

function afficher($a){
    echo '<p>' , $a , '</p>';

}

// Exercice 1
// $mois = array(1, 2, 3); ancienne version

$mois = [
    1 => "Janvier",
    2 => "Février",
    3 => "Mars",
    4 => "Avril",
    5 => "Mai",
    6 => "Juin",
    7 => "Juillet",
    8 => "Aout",
    9 => "Septembre",
    10 => "Octobre",
    11 => "Novembre",
    12 => "Décembre",
];

// Exercice 2

afficher( $mois[2]);


//  Exercice 3

afficher ($mois[5]);


// Exercice 4

// $mois[7] = "Août"; //attention on a changé les index donc aout devient index 8

// Exercice 7

// $mois[13] = "Fleurence";
// afficher ($mois[13]);



// Exercice 8

for($i = 1 ; $i <= 12; $i++){ // count($mois) pareil c la taille

    afficher ($mois[$i]);
}

foreach($mois as $i => $value){

    afficher ("Le mois de ".$value. " a l'index ".$i);

}

?>





    
</body>
</html>